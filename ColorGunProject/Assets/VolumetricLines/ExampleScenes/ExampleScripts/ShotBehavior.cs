﻿using UnityEngine;
using System.Collections;

public class ShotBehavior : Projectile {

	private VolumetricLines.VolumetricLineBehavior laser;
	private Color laserColor;

	protected override void Awake() {
		laser = this.GetComponent<VolumetricLines.VolumetricLineBehavior>();
		laserColor = laser.LineColor;
		base.Awake();
	}

	// Use this for initialization
	void Start () {
		CapsuleCollider collider = gameObject.AddComponent<CapsuleCollider>();
		collider.direction = 2;
		collider.radius = 0.035f;
		collider.height = 0.6f;
	}
	
	// Update is called once per frame
	void Update () {	
	}

	public override Color GetColor() {
		return laserColor;
	}

	public override void ChangeColor(Color newColor) {
		laser.LineColor = newColor;
		laserColor = newColor;
	}
}
