﻿using UnityEngine;
using System.Collections;

public class GunController : MonoBehaviour {

	void Awake() 
	{
		gameObject.GetComponentsInChildren<GunLaser>();
		gameObject.GetComponentsInChildren<ChargeGun>();
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
