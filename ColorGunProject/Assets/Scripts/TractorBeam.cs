using UnityEngine;
using System.Collections;

public class TractorBeam : MonoBehaviour {

	private VolumetricLines.VolumetricLineBehavior laser;
	private MeshRenderer laserRenderer;
	private Color laserColor;

	private bool hitSomething = false;	// If dot was hit on button down
	private Dot hitDot = null;	// Holds the dot that was hit
	private Rigidbody hitDotRb;
	private GameObject plane;	// The plane that gets created when a dot is hit, used for raycast

	// Use this for initialization
	void Awake () {
 
		// Access the laser behavior
		laser = gameObject.GetComponentInChildren<VolumetricLines.VolumetricLineBehavior>();

		// Set the initial color of the laser
		laserColor = laser.LineColor;

		// Access the renderer for the laser
		laserRenderer = gameObject.GetComponentInChildren<MeshRenderer>();

		laserRenderer.enabled = false; 

		laserRenderer.sortingOrder = 7;
	
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		// Register if the player has tapped the dot
		if (Input.GetMouseButtonDown(0)) 
		{

			Vector3 tapPosition = Input.mousePosition;

			Ray ray = Camera.main.ScreenPointToRay(tapPosition);
			RaycastHit2D hitInfo = Physics2D.GetRayIntersection(ray);

			if(hitInfo.transform != null)
			{
				Debug.Log(hitInfo.transform);
				hitDot = hitInfo.transform.parent.gameObject.GetComponent<Dot>();
				hitDot.ToggleTractorBeam(); 	// Turn on tractor beam logic for the obstacle
				hitDotRb = hitDot.transform.gameObject.GetComponent<Rigidbody>();
				hitDotRb.isKinematic = true;	// Turn on kinematic to register velocity
				CreatePlane(hitDot.transform.position.z);
			}
		}

		// Follow the cursor while button is held
		if (Input.GetMouseButton(0))
		{

			Vector3 tapPosition = Input.mousePosition;
			tapPosition.z = 10;
			Vector3 worldPosition = transform.InverseTransformPoint(Camera.main.ScreenToWorldPoint(tapPosition));

			laser.EndPos = worldPosition;
			laser.LineWidth = 0.1f;
			laserRenderer.enabled = true;

			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit2D hit = Physics2D.GetRayIntersection(ray);

			if (hitDot != null) {
				hitDot.StopGrowing();
				Vector2 newPosition = hit.point;
				hitDotRb.MovePosition(newPosition);				
			}


		} else {
			DeletePlane();
			laserRenderer.enabled = false;
		}

		if (Input.GetMouseButtonUp(0) && hitDot != null) 
		{
			hitDot.ToggleTractorBeam();		// activate tractor beam logic for obstacle
			Vector2 force = hitDotRb.velocity;
			hitDotRb.isKinematic = false;	// Turn off kinematic to add force
			hitDotRb.AddForce(force, ForceMode.Impulse);
			DeletePlane();
			hitDot.KeepGrowing();
			hitSomething = false;
			hitDot = null;
		}
	}

	// This function creates a box collider to be used as a plane for raycasting
	void CreatePlane(float zPosition)
	{
		plane = (GameObject) Instantiate(Resources.Load("Plane"), new Vector3(0,0,zPosition) , Quaternion.Euler(0.0f, 0.0f, 0.0f));
	}

	void DeletePlane()
	{
		Destroy(plane);
	}


}
