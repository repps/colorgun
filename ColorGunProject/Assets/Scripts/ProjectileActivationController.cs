﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileActivationController : MonoBehaviour {

	public float timeToActivateObjectInSeconds;

	private Projectile projectileToActivate;
	private SpriteRenderer projectileRenderer;
	private Collider projectileCollider;

	// Use this for initialization
	void Awake () {

		// Setting the object activation to false initially
		projectileToActivate = this.gameObject.GetComponent<Projectile>();
		projectileRenderer = this.gameObject.GetComponent<SpriteRenderer>();
		projectileCollider = this.gameObject.GetComponent<Collider>();
		projectileCollider.enabled = false;
		projectileRenderer.enabled = false;
		projectileToActivate.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (Time.timeSinceLevelLoad > timeToActivateObjectInSeconds) {
			projectileCollider.enabled = true;
			projectileRenderer.enabled = true;
			projectileToActivate.enabled = true;
		}
	}
}
