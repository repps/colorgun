﻿using UnityEngine;
using System.Collections;
using TMPro;

public class Weapon : MonoBehaviour {

	protected VolumetricLines.VolumetricLineBehavior laser;
	protected float nextShot;
	protected Transform muzzle;
	
	public Color laserColor;
	public GameObject shotPrefab;
	public float shotCooldown;
	public int ammo;

	private TextMeshProUGUI ammoUI;

	// Use this for initialization
	protected virtual void Awake () {
	
		muzzle = this.transform.GetChild(0);
		ammoUI = this.GetComponentInChildren(typeof(TextMeshProUGUI)) as TextMeshProUGUI;
		ammoUI.text = ammo.ToString();

	}

	protected virtual void Fire () {

	}
	
	// Update is called once per frame
	protected virtual void Update () {
		Fire();
	}

	protected void UpdateAmmo() {
		ammo--;
		ammoUI.text = ammo.ToString();
	}
}
