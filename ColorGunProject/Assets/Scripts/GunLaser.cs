﻿using UnityEngine;
using System.Collections;

public class GunLaser : Weapon {

	public float shotForce;

	// Use this for initialization
	protected override void Awake () {
		base.Awake();
	}
	
	// Update is called once per frame
	protected override void Update () {
		base.Update();
	}

	protected override void Fire () {
		// Check collision against UI layer
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		bool hitUiElement = Physics.Raycast(ray,100,~1);

		// Touch input
		if (Input.touchCount > 0 && Time.timeSinceLevelLoad > nextShot && !hitUiElement && base.ammo > 0)
		{
			base.UpdateAmmo();
			nextShot = Time.timeSinceLevelLoad + shotCooldown;

			Vector3 tapPosition = Input.GetTouch(0).position;
			tapPosition.z = 10;
			Vector3 worldPosition = Camera.main.ScreenToWorldPoint(tapPosition);

			muzzle.LookAt(worldPosition, Vector3.up);

			GameObject shot = GameObject.Instantiate(shotPrefab, new Vector3(this.transform.position.x, this.transform.position.y, 0), muzzle.localRotation) as GameObject;
			shot.GetComponent<Projectile>().ChangeColor(laserColor);
			Rigidbody shotRB = shot.GetComponent(typeof(Rigidbody)) as Rigidbody;	
			shotRB.AddForce(shot.transform.forward * shotForce);		
			GameObject.Destroy(shot, 3f);

		}

		// Mouse input, mostly for testing

		if (Input.GetMouseButtonDown(0) && Time.timeSinceLevelLoad > nextShot && !hitUiElement && base.ammo > 0 && Application.isEditor)
		{
			base.UpdateAmmo();
			nextShot = Time.timeSinceLevelLoad + shotCooldown;

			Vector3 tapPosition = Input.mousePosition;

			tapPosition.z = 10;
			Vector3 worldPosition = Camera.main.ScreenToWorldPoint(tapPosition);

			muzzle.LookAt(worldPosition, Vector3.up);

			GameObject shot = GameObject.Instantiate(shotPrefab, new Vector3(this.transform.position.x, this.transform.position.y, 0), muzzle.localRotation) as GameObject;
			shot.GetComponent<Projectile>().ChangeColor(laserColor);
			Rigidbody shotRB = shot.GetComponent(typeof(Rigidbody)) as Rigidbody;
			shotRB.AddForce(shot.transform.forward * shotForce);
			GameObject.Destroy(shot, 3f);

		}
	}
}
