﻿using UnityEngine;
using System.Collections;

public class TapToDestroy : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		// Mutli Touch input
		if (Input.touchCount > 0)
		{
			foreach(Touch touch in Input.touches) 
			{
				Ray ray = Camera.main.ScreenPointToRay(touch.position);
				RaycastHit2D hit = Physics2D.GetRayIntersection(ray);

				if (hit.transform != null) 
				{
					if (hit.transform.gameObject.GetInstanceID() == gameObject.GetInstanceID())
					{
						Destroy(hit.transform.gameObject);
					}
				}
			}
		}
	}
}
