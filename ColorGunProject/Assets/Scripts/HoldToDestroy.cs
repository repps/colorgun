﻿using UnityEngine;
using System.Collections;

public class HoldToDestroy : MonoBehaviour {

	private float startTouchTime;
	private float timePressed;
	private bool pressed;
	private CircleCollider2D collider;

	private Dot thisDot;

	public float holdTime;

	// Use this for initialization
	void Awake () {
		
		// Access the Meteor object
		thisDot = gameObject.GetComponentInChildren<Dot>();

		// Access this objects collider and set to false initially
		collider = GetComponentInChildren<CircleCollider2D>();
		collider.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
	
		if (Input.touchCount > 0 && transform.childCount == 0)
		{
			// enable the collider only if all of the hit points have been destroyed
			collider.enabled = true;

			foreach(Touch touch in Input.touches) 
			{
				Ray ray = Camera.main.ScreenPointToRay(touch.position);
				RaycastHit2D hit = Physics2D.GetRayIntersection(ray);

				// Get the time the user has tapped the meteor
				if (touch.phase == TouchPhase.Began && hit.transform != null && !pressed) 
				{
					if (hit.transform.gameObject.GetInstanceID() == gameObject.GetInstanceID())
					{
						thisDot.StopGrowing();

						startTouchTime = Time.timeSinceLevelLoad;
						pressed = true;
					}

				}

				// Check if the user is still holding 
				if(hit.transform != null && pressed) 
				{
					if (Time.timeSinceLevelLoad - startTouchTime > holdTime 
						&& hit.transform.gameObject.GetInstanceID() == gameObject.GetInstanceID())
					{
						Destroy(hit.transform.gameObject);
						pressed = false;
					}
				}

				// if the user has lifted finger, then reset the boolean
				if (touch.phase == TouchPhase.Ended && pressed) 
				{
					pressed = false;
					thisDot.KeepGrowing();
				}

			}
		} else {

		}

	}
}
