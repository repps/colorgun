﻿using UnityEngine;
using System.Collections;

public abstract class Projectile : MonoBehaviour {

	// This is the ratio that is used to keep the size between the particles and the projectile consistent
	private float particleToDotRatio = 12.5f;
	private Rigidbody rb;
	private Vector3 oldVelocity;

	public abstract Color GetColor();

	public abstract void ChangeColor(Color newColor);

	protected bool IsEqualTo(Color color, Color other) {
        return color.r == other.r && color.g == other.g && color.b == other.b && color.a == other.a;
	}

	protected bool IsBaseColor(Color color) {
		return (IsEqualTo(color,Color.red) || IsEqualTo(color, Color.blue)  || IsEqualTo(color,Color.green));
	}

	public virtual void OnTriggerEnter(Collider other) {
		if (other.tag == "Obstacle") {
			Projectile otherProj = other.gameObject.GetComponent<Projectile>();
			Color otherColor = otherProj.GetColor();
			if (IsEqualTo(this.GetColor(), otherColor)) {
				GameObject particles = Resources.Load("Particles") as GameObject;
				particles.transform.localScale = (other.transform.localScale / particleToDotRatio);
				ParticleSystem pSystem = particles.GetComponent<ParticleSystem>();
				var pMain = pSystem.main;
				pMain.startColor = otherColor;
				GameObject activeParticles = Instantiate(particles, other.transform.position, Quaternion.Euler(0.0f, 0.0f, 0.0f)) as GameObject;
				Destroy(activeParticles, 3);
				Destroy(this.gameObject);
				Destroy(other.transform.gameObject);
			} else if (IsBaseColor(this.GetColor()) && IsBaseColor(otherColor)) {
				otherProj.ChangeColor(otherColor + this.GetColor());
				Destroy(this.gameObject);
			} else {
				Destroy(this.gameObject);				
			}
		}
	}

	protected virtual void Awake() {
		rb = this.GetComponent(typeof(Rigidbody)) as Rigidbody;
	}

	void FixedUpdate() {
		oldVelocity = rb.velocity;
	}

	public virtual void OnCollisionEnter(Collision collision) {
        // get the point of contact
         ContactPoint contact = collision.contacts[0];

         // reflect our old velocity off the contact point's normal vector
         Vector3 reflectedVelocity = Vector3.Reflect(oldVelocity, contact.normal);        

         // assign the reflected velocity back to the rigidbody
         rb.velocity = reflectedVelocity;
         // rotate the object by the same ammount we changed its velocity
         Quaternion rotation = Quaternion.FromToRotation(oldVelocity, reflectedVelocity);
         this.transform.rotation = rotation * this.transform.rotation;
         rb.angularVelocity = new Vector3(0,0,0);

         //Set the collider to isTrigger, so that it cannot bounce off another edge.
         this.gameObject.GetComponent<Collider>().isTrigger = true; 


	}
}
