﻿using UnityEngine;
using System.Collections;

public class ChargeGun : Weapon {

	private bool instantiated;
	private GameObject chargeShot = null;

	public float shotForce;
	public float growSpeed;

	// Use this for initialization
	protected override void Awake () {
		base.Awake();
	}

	void Start() {

	}
	
	// Update is called once per frame
	protected override void Update () {
		base.Update();
	}

	protected override void Fire () {
		// Check collision against UI layer
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		bool hitUiElement = Physics.Raycast(ray,100,~1);

		// Touch input
		if (Input.touchCount > 0 && Time.timeSinceLevelLoad > nextShot && !hitUiElement && base.ammo > 0)
		{
			nextShot = Time.timeSinceLevelLoad + shotCooldown;

			Touch touch = Input.GetTouch(0);
			Vector3 tapPosition = touch.position;
			tapPosition.z = 10;
			Vector3 worldPosition = Camera.main.ScreenToWorldPoint(tapPosition);

			// Rotates the up vector to point towards the target
			// ????? Rotate muzzle only and not entire transform ??????
			muzzle.transform.up = worldPosition - muzzle.transform.position;

			// Ensures the shot's instatiation is dependent on the rotation of the transform
			Vector3 shotPosition = this.transform.position + (transform.up/2);

			if (!instantiated && touch.phase != TouchPhase.Ended) 
			{
				chargeShot = GameObject.Instantiate(shotPrefab, shotPosition, muzzle.rotation) as GameObject;
				chargeShot.transform.SetParent(this.transform, true);
				chargeShot.GetComponent<Projectile>().ChangeColor(laserColor);
				instantiated = true;
			}
			
			// Increase size of shot while holding
			if (instantiated && chargeShot != null && touch.phase != TouchPhase.Ended) 
			{
				chargeShot.transform.rotation = muzzle.rotation;
				Shake(chargeShot.transform);
				chargeShot.transform.localScale += new Vector3(growSpeed * Time.deltaTime, growSpeed * Time.deltaTime, growSpeed * Time.deltaTime);
				Vector3 difference = worldPosition - this.transform.position;
 				float rotationZ = Mathf.Atan2(difference.x, difference.y) * Mathf.Rad2Deg;
 				this.transform.rotation = Quaternion.Euler(0.0f, 0.0f, -rotationZ);
			}

			if (touch.phase == TouchPhase.Ended) {
				base.UpdateAmmo();
				instantiated = false;
				chargeShot.transform.parent = null;
				chargeShot.transform.position = new Vector3(chargeShot.transform.position.x, chargeShot.transform.position.y, 0);
				Rigidbody chargeShotRB = chargeShot.GetComponent(typeof(Rigidbody)) as Rigidbody;
				chargeShotRB.velocity = new Vector3(0,0,0);
				chargeShotRB.AddForce(chargeShotRB.transform.up * shotForce);
				GameObject.Destroy(chargeShot, 5f);				
			}

		}

		// Mouse input, mostly for testing
		if (Input.GetMouseButton(0) && !hitUiElement && base.ammo > 0 && Application.isEditor)
		{
			nextShot = Time.timeSinceLevelLoad + shotCooldown;

			Vector3 tapPosition = Input.mousePosition;
			tapPosition.z = 10;
			Vector3 worldPosition = Camera.main.ScreenToWorldPoint(tapPosition);

			// Rotates the up vector to point towards the target
			// ????? Rotate muzzle only and not entire transform ??????
			muzzle.transform.up = worldPosition - muzzle.transform.position;

			// Ensures the shot's instatiation is dependent on the rotation of the transform
			Vector3 shotPosition = this.transform.position + (transform.up/2);

			if (!instantiated) 
			{
				chargeShot = GameObject.Instantiate(shotPrefab, shotPosition, muzzle.rotation) as GameObject;
				chargeShot.transform.SetParent(this.transform, true);
				chargeShot.GetComponent<Projectile>().ChangeColor(laserColor);	
				instantiated = true;
			}
			
			// Increase size of shot while holding
			if (instantiated && chargeShot != null) 
			{
				chargeShot.transform.rotation = muzzle.rotation;
				Shake(chargeShot.transform);
				chargeShot.transform.localScale += new Vector3(growSpeed * Time.deltaTime, growSpeed * Time.deltaTime, growSpeed * Time.deltaTime);
				Vector3 difference = worldPosition - this.transform.position;
 				float rotationZ = Mathf.Atan2(difference.x, difference.y) * Mathf.Rad2Deg;
 				this.transform.rotation = Quaternion.Euler(0.0f, 0.0f, -rotationZ);
			}

		}

		if (Input.GetMouseButtonUp(0) && !hitUiElement && base.ammo > 0 && Application.isEditor)
		{
			base.UpdateAmmo();
			instantiated = false;
			chargeShot.transform.parent = null;
			chargeShot.transform.position = new Vector3(chargeShot.transform.position.x, chargeShot.transform.position.y, 0);
			Rigidbody chargeShotRB = chargeShot.GetComponent(typeof(Rigidbody)) as Rigidbody;
			chargeShotRB.velocity = new Vector3(0,0,0);
			chargeShotRB.AddForce(chargeShotRB.transform.up * shotForce);
			GameObject.Destroy(chargeShot, 5f);
		}
	}

	void Shake(Transform transform) {
		transform.position += new Vector3(PingPong(Time.time, -0.025f, 0.025f), PingPong(Time.time, -0.025f, 0.025f), 0);
	}

	 float PingPong(float aValue, float aMin, float aMax) {
	     return Mathf.PingPong(aValue, aMax-aMin) + aMin;
	 }

}
