﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LevelManagerScript : MonoBehaviour {

	public static LevelManagerScript instance;		//Only hold one instance of the level Manager

	// Use this for initialization
	void Start () {


		// Do not destroy this game object
		// Ensure that there is always one instance of this object
		DontDestroyOnLoad(gameObject);

		if (instance == null)
		{
			instance = this;
		}
		else if (instance != this)
		{
			GameObject.Destroy(gameObject);
		}

		Object.DontDestroyOnLoad(gameObject);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void LoadScene(int level) {
		SceneManager.LoadScene(level);
	}
}
