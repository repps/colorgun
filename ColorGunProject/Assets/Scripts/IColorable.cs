﻿using UnityEngine;
using System.Collections;

public abstract class IColorable : MonoBehaviour {

	protected abstract Color GetColor();

	protected abstract void ChangeColor(Color newColor);

	bool IsEqualTo(Color color, Color other) {
        return color.r == other.r && color.g == other.g && color.b == other.b && color.a == other.a;
	}

}
