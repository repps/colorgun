﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScrollRectSnap_CS : MonoBehaviour {

	public RectTransform panel; // This is the scroll panel
	public Image[] images;
	public RectTransform center; // Center to compare the distance of each button

	private float[] distance;	// All of the buttons distance to the center
	private bool dragging = false;	// Will be true while dragging the panel
	private int imageDistance;	// Will hold the distance between the buttons
	private int minImageNum;	// The index of the button that is closest to the center
	private Weapon activeLaser; // The active gun laser
	private ChargeGun activeChargeGun; // The active charge gun

	// Use this for initialization
	void Start () {

		int imagesLength = images.Length;
		distance = new float[imagesLength];

		imageDistance = (int) Mathf.Abs(images[1].GetComponent<RectTransform>().anchoredPosition.x - images[0].GetComponent<RectTransform>().anchoredPosition.x);
	}
	
	// Update is called once per frame
	void Update () {
	
		for (int i = 0; i < images.Length; i++) {
			distance[i] = Mathf.Abs(center.transform.position.x - images[i].transform.position.x);
		}

		float minDistance = Mathf.Min(distance);

		for (int a = 0; a < images.Length; a++) {

			if (minDistance == distance[a]) {
				minImageNum = a;
				activeLaser = images[a].gameObject.GetComponent<Weapon>();
				images[a].CrossFadeAlpha(1.0f, 0.5f, false);
				activeLaser.enabled = true;	// Set the laser to be active
			} else {
				Weapon otherLaser = images[a].gameObject.GetComponent<Weapon>();
				images[a].CrossFadeAlpha(0.2f, 1.0f, false);
				otherLaser.enabled = false;	// Turn off all other lasers
			}

		}

		if (!dragging)
		{
			LerpToImage(minImageNum * -imageDistance);
		}

	}

	void LerpToImage(int position)
	{
		float newX = Mathf.Lerp(panel.anchoredPosition.x, position, Time.deltaTime * 10f);
		Vector2 newPosition = new Vector2(newX, panel.position.y);

		panel.anchoredPosition = newPosition;
	}

	public void StartDrag()
	{
		dragging = true;
	}

	public void EndDrag()
	{
		dragging = false;
	}
}
