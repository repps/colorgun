﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorEdgeScript : MonoBehaviour {

	private Color edgeColor;
	private SpriteRenderer edgeRenderer;

	// Use this for initialization
	void Awake () {
		edgeRenderer = GetComponent<SpriteRenderer>();
		edgeColor = edgeRenderer.color;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter (Collision collision) {
		Projectile projectile = collision.gameObject.GetComponent<Projectile>();
		if (projectile != null) {
			Color newColor = (projectile.GetColor() + this.edgeColor);
			projectile.ChangeColor(newColor);
		}
	}
}
