﻿using UnityEngine;
using System.Collections;

public class ChargeShotBehavior : Projectile {

	private Color shotColor;
	private SpriteRenderer shotRenderer;
	private bool fired = false;			// Not set to true until mouse is released
	private Animator animator;

	// Use this for initialization
	protected override void Awake () {
		shotRenderer = GetComponent<SpriteRenderer>();
		shotColor = shotRenderer.color;
		base.Awake();
	}
	
	public override void OnTriggerEnter(Collider other) {
		if (other.tag == "Obstacle") {
			float otherSize = other.transform.localScale.magnitude;
			float thisSize = this.transform.localScale.magnitude;
			if (thisSize > otherSize) {
				base.OnTriggerEnter(other);
			}
		}
	}

	public override Color GetColor() {
		return shotColor;
	}

	public override void ChangeColor(Color newColor) {
		shotRenderer.color = newColor;
		shotColor = newColor;
	}
}
