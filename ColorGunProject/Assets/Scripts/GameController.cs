﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

	private GameObject background;
	private GameObject mainCamera;
	private int meteorCount;
	private float nextMeteor;
	private bool gameOver = false;
	private bool restart = false;
	private GameObject[] livesArray;

	public float meteorRespawn;
	public float startWait;
	public int lives;
	public GameObject redFlash;
	public GameObject gameOverText;
	public GameObject healthCanvas;
	public bool randomlySpawn;

	// Use this for initialization
	void Start () {

		// Ensure that the frame rate of our game is 60, on iOS devices
        Application.targetFrameRate = 60;

        // Grab the background gameObject
        background = GameObject.FindWithTag("Background");

        // Grab the camera gameObject
        mainCamera = GameObject.FindWithTag("MainCamera");

        // Set the meteor count to 0
        meteorCount = 0;

        // Instantiate all of the lives during instantiation
        livesArray = new GameObject[lives];
        Vector3 startPosition = new Vector3(0, 0, 0);
        for (int i = 0; i < lives; i++) {
        	GameObject life = Instantiate(Resources.Load("Life") ,healthCanvas.transform, false) as GameObject;
        	livesArray[i] = life;
        	life.transform.position += startPosition;
        	startPosition += Camera.main.ScreenToViewportPoint(new Vector3(-150f, 0, 0));
        }

		if (randomlySpawn) {
        	StartCoroutine (SpawnWaves ());
		} 	
	}
	
	// Update is called once per frame
	void Update () {
        if (restart)
        {
            if (Input.GetKeyDown (KeyCode.R))
            {
				SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
        }
	}

	IEnumerator Fade(float start, float end, float length, GameObject currentObject) {
		if (currentObject.GetComponent<Image>().color.a == start){
			for (float i = 0.0f; i < 1.0f; i += Time.deltaTime*(1/length)) { //for the length of time
				Color guiColor = currentObject.GetComponent<Image>().color;
				currentObject.GetComponent<Image>().color = new Color(guiColor.r, guiColor.g, guiColor.b, Mathf.Lerp(start, end, i)); //lerp the value of the transparency from the start value to the end value in equal increments
				yield return 0;
				currentObject.GetComponent<Image>().color = new Color(guiColor.r, guiColor.g, guiColor.b, end); // ensure the fade is completely finished (because lerp doesn't always end on an exact value)
        	} //end for
		} //end if

	}

	IEnumerator FlashWhenHit() {
		StartCoroutine(Fade (0f, 0.4f, 0.3f, redFlash));
    	yield return new WaitForSeconds(0.5f);
    	StartCoroutine(Fade (0.4f, 0f, 0.3f, redFlash));
	}

    IEnumerator SpawnWaves ()
    {
        yield return new WaitForSeconds (startWait);
        while (true)
        {
            while (!gameOver)
            {
				bool clearSpawn = false;
				Vector2 spawnPosition;
				Vector2 randomForce;

				// Ensure to not spawn dots on top of one another
				do
				{ 	
					float midHeight = Screen.height/2;
					float midWidth = Screen.width/2;
					Vector2 pixelPoint = new Vector2(Random.Range(0,Screen.width), Random.Range(220,Screen.height));
					Vector3 screenPosition = Camera.main.ScreenToWorldPoint(pixelPoint);
					Vector3 endCapsulePosition = new Vector3(screenPosition.x, screenPosition.y, 0);
					spawnPosition = screenPosition;

					if (pixelPoint.x <= midWidth && pixelPoint.y <= midHeight) 
					{
						randomForce = new Vector2(Random.Range(-0.2f, 0), Random.Range(-0.2f, 0));
					} else if (pixelPoint.x <= midWidth && pixelPoint.y >= midHeight) 
					{
						randomForce = new Vector2(Random.Range(-0.2f, 0), Random.Range(0, 0.2f));
					} else if (pixelPoint.x >= midWidth && pixelPoint.y >= midHeight) 
					{
						randomForce = new Vector2(Random.Range(0, 0.2f), Random.Range(0, 0.2f));
					} else if (pixelPoint.x >= midWidth && pixelPoint.y <= midHeight) 
					{
						randomForce = new Vector2(Random.Range(0, 0.2f), Random.Range(-0.2f, 0));
					} else {
						randomForce = Vector2.zero;
					}

					if (!Physics.CheckCapsule(screenPosition, endCapsulePosition, 0.1f))
					{
						clearSpawn = true;
					}

				} while (clearSpawn == false);

				int meteorType = Random.Range(0, 3); 

				switch (meteorType)
				{
					case 0:
						GameObject dot = (GameObject) Instantiate(Resources.Load("Dot"), Vector3.zero, Quaternion.Euler(0.0f, 0.0f, 0.0f));
						dot.GetComponent<Rigidbody>().AddForce(Vector3.zero, ForceMode.Impulse);
						break;
					case 4:
						GameObject holdMeteor = (GameObject) Instantiate(Resources.Load("HoldMeteor"), spawnPosition, Quaternion.Euler(0.0f, 0.0f, 0.0f));
						break;
					case 3:
						GameObject tapHoldMeteor = (GameObject) Instantiate(Resources.Load("TapHoldMeteor"), new Vector2(0f, 0f), Quaternion.Euler(0.0f, 0.0f, 0.0f));
						nextMeteor += 5;
						break;
					case 1:
						GameObject blueDot = (GameObject) Instantiate(Resources.Load("Dot"), Vector3.zero, Quaternion.Euler(0.0f, 0.0f, 0.0f));
						blueDot.GetComponent<Dot>().ChangeColor(Color.blue);
						blueDot.GetComponent<Rigidbody>().AddForce(Vector3.zero, ForceMode.Impulse);
						break;
					case 2:
						GameObject greenDot = (GameObject) Instantiate(Resources.Load("Dot"), Vector3.zero, Quaternion.Euler(0.0f, 0.0f, 0.0f));
						greenDot.GetComponent<Dot>().ChangeColor(Color.green);
						greenDot.GetComponent<Rigidbody>().AddForce(Vector3.zero, ForceMode.Impulse);
						break;

				}

                yield return new WaitForSeconds (meteorRespawn);

            }
            //yield return new WaitForSeconds (waveWait);

            restart = true;
            break;
        }
    }


	public void TakeDamage() 
	{
		StartCoroutine(FlashWhenHit());
		Destroy(livesArray[lives-1]);
		lives--;
		if (lives < 1) {
			gameOver = true;
			gameOverText.SetActive(true);
			restart = true;
		}
	}


}
