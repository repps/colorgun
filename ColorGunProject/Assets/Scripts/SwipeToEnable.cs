﻿using UnityEngine;
using System.Collections;

public class SwipeToEnable : MonoBehaviour {

	private GunLaser gunLaser;

	private GunLaser otherLaser;

	// Use this for initialization
	void Awake () {

		gunLaser = gameObject.GetComponent<GunLaser>();

		otherLaser = GameObject.Find("Gun").GetComponent<GunLaser>();

		gunLaser.enabled = false;
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnMouseEnter() {
		ToggleLaser();
	}

	private void ToggleLaser() {
		gunLaser.enabled = !gunLaser.enabled;
		otherLaser.enabled = !otherLaser.enabled;
	}
}
