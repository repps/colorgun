using UnityEngine;
using System.Collections;

public class Dot : Projectile {

	private GameController gameController;
	private GameObject mainCamera;
	private Rigidbody meteorRb;
	private float moveTowards;
	private SpriteRenderer dotRenderer;
	private bool keepGrowing;
	private Color dotColor;
	private bool tractorBeamActive = false;
	
	public Vector3 targetScale;
	public float speed;

	// Use this for initialization
	protected override void Awake () {
		// Call base awake to initialize rigidbody etc
		base.Awake();
		
		// Access the main camera object
		mainCamera = GameObject.FindWithTag("MainCamera");

		Input.simulateMouseWithTouches = true;

		// Access the renderer
		dotRenderer = GetComponent<SpriteRenderer>();

		// Set the dot color
		dotColor = dotRenderer.color;

		// Boolean to control the movement of the meteor
		keepGrowing = true;

		// Access the game controller
		gameController = GameObject.FindWithTag("GameController").GetComponent<GameController>();

	}
	
	// Update is called once per frame
	void Update () {
	
		if (keepGrowing)
		{
			this.transform.localScale += new Vector3(speed * Time.deltaTime, speed * Time.deltaTime, speed * Time.deltaTime);
		}

		float scaleDifference = targetScale.x - this.transform.localScale.x;

		if (scaleDifference < 0.1)
		{
			gameController.TakeDamage();
			Destroy(gameObject);
		}


		// if (!dotRenderer.isVisible) 
		// {
		// 	Destroy(gameObject);
		// }

	}

	public void StopGrowing()
	{
		keepGrowing = false;
	}

	public void KeepGrowing() 
	{
		keepGrowing = true;
	}

	public override void OnTriggerEnter(Collider other) {
		// Only add collision logic if tractor beam is active
		if (tractorBeamActive) {
			base.OnTriggerEnter(other);
		}
	}

	public override void OnCollisionEnter(Collision collision) {
		// Do nothing let the fireable projectiles handle this
	}

	public override Color GetColor()
	{
		return dotColor;
	}

	public override void ChangeColor(Color newColor)
	{
		dotColor = newColor;
		dotRenderer.color = dotColor;
	}

	public void ToggleTractorBeam() {
		this.tractorBeamActive = !this.tractorBeamActive;
	}

}
